package com.example.undertow.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUndertowDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUndertowDemoApplication.class, args);
    }

}
