package com.example.undertow.demo.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 默认使用utf-8字符集
 */
public class Utf8EncodingFilter extends GenericFilterBean {

    private static Logger logger = LoggerFactory.getLogger(Utf8EncodingFilter.class);


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        Utf8EncodingRequest utf8EncodingRequest =new Utf8EncodingRequest((HttpServletRequest)request);
        filterChain.doFilter(utf8EncodingRequest, response);
    }

    protected void initFilterBean() throws ServletException {
        logger.info(">>>>>>>>>>>>>加载Utf8EncodingFilter!");
    }

}