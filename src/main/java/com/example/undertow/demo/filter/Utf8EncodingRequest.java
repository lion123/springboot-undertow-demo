package com.example.undertow.demo.filter;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * 默认使用utf-8字符集
 * 解决使用undertow作为web容器带来的中文乱码的问题
 */
public class Utf8EncodingRequest extends HttpServletRequestWrapper {

    public Utf8EncodingRequest(HttpServletRequest request) {
        super(request);
    }

    @Override
    public String getCharacterEncoding() {
        String characterEncoding =this.getRequest().getCharacterEncoding();
        return StringUtils.isBlank(characterEncoding)?"utf-8":characterEncoding;
    }

}