package com.example.undertow.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by liunanhua on 2018/3/19.
 */
@Configuration
public class BaseConfig {


    @Bean(name = "hostName")
    public String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "undertow-demo";
    }


}
