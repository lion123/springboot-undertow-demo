package com.example.undertow.demo.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;

import java.nio.charset.StandardCharsets;

@Configuration
public class CharacterEncodingFilterConfig {

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        //用于注册过滤器
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        // 使用spring 提供的字符编码过滤器，不用自己写过滤器
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter(StandardCharsets.UTF_8.toString(), true);
        filterRegistrationBean.setFilter(characterEncodingFilter);
        return filterRegistrationBean;
    }


}
