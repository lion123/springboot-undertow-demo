package com.example.undertow.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by liulanhua on 2018/9/25.
 */
@RestController
public class IndexController {


    @GetMapping(value = "")
    public ModelAndView index(ModelAndView mv) {
        mv.setViewName("index");
        mv.addObject("record", "Hello, Please login !");
        return mv;
    }


}
